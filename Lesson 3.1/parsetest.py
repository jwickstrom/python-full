import argparse

addition = lambda x,y : x+y
subtraction = lambda x,y : x-y

def checkname(name):
    if "e" not in name:
        raise argparse.ArgumentTypeError("Must have E")
    return name


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("x", help="An integer value that will be used in our math operator. Pos X", type=int)    
    parser.add_argument("y", type=int) # Two identifiers, to put inside our command-line
    
    parser.add_argument("z", type=checkname)
    parser.add_argument("-s", "--subtract", action="store_true")    
    
    args = parser.parse_args()
    print(args)
    
    print(args.subtract) # long format, equal to the one above!!
    
    x = args.x
    y = args.y #as to exactly match the one above
    
    # Will not neeed this anymore, when typecast instatly
    #x, y = int(x), int (y)
    
    print("x =", x)
    print("y =", y)
    
    print("Addition:", addition(x, y))
    
    if args.subtract == True: # Make it print or not if True/False, the -s flag.
        print("Subtraction:", subtraction(x, y))