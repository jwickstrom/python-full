#!/usr/bin/env python
# coding: utf-8

# In[1]:

import turtle
import argparse # Import argparse instead of sys. 
from matplotlib.colors import is_color_like # Import to validate the colors in checkColor

s = turtle.getscreen() 
t = s.turtles()[0]

s.title("Turtle Program!")
t.pen(shown=False)

def checkType(input):   
    if input == "square" or input == "circle" or input == "triangle":
        return input
    else:
        print("The chosen shape is not yet available.. Here, let me write u a square instead!") # Warn them with a print statement!       
        return "square" # Returns a "default" shape
    
def checkColor(color_in):
    if is_color_like(color_in):
        return color_in
    else:
        print("Wrong color, lemme blacken it!")
        return "black"
    
    
if __name__ =="__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--color", type=checkColor, help="If you have personal preference for color, default is RED", default = "red")
    parser.add_argument("shape", type=checkType, help="Which shape u want? square, circle and triangle is available.")    
    #parser.add_argument("shape", choices=['square', 'circle', 'triangle'], help="Which shape u want? square, circle and triangle is available.")    

    parser.add_argument("-v", "--verbose", action="store_true") # A boolean option for more information to the user.
    
    args = parser.parse_args()
    color = args.color # Set the color to the CLA # Red if no color was given (optional)
    shape = args.shape # Set the shape to the CLA # Square if the input was not written correctly
            
    t.pensize(2)
    t.fillcolor(color)
    
    if args.verbose:
            print(f"The chosen shape is {shape} and the color is {color}")
    if (shape=="square"):
        t.begin_fill()
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.right(90)
        t.forward(100)
        t.end_fill()
        
    elif (shape=="circle"):
        t.begin_fill()
        t.circle(50)
        t.end_fill()

    elif (shape=="triangle"):
        t.begin_fill()
        t.forward(100)
        t.right(120)
        t.forward(100)
        t.right(120)
        t.forward(100)
        t.end_fill()
    
    else:
        print("Unknown shape!")

    turtle.done()
    quit()

