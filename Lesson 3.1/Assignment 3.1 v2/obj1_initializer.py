import argparse

parser = argparse.ArgumentParser()
parser.add_argument("a") # Three identifiers, to put inside our command-line
parser.add_argument("b")
parser.add_argument("c")

args = parser.parse_args()
print(args)

a = args.a
b = args.b
c = args.c
print(a, b, c)