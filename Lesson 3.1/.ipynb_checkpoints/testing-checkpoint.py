import sys
args = sys.argv[1:]
print(args)
addition = lambda x,y : x+y
subtraction = lambda x,y : x-y

# Main loop
if __name__ == '__main__':
    x, y, *etc = tuple(sys.argv[1:])
    print("x", x)
    print("y", y)
    
    print("Addition:", addition(int(x), int(y)))
    print("Subtraction:", subtraction(int(x), int(y)))