import webbrowser # Module allowing for quick display of websites. 
import ViewCatalog # Fetching the catalog so that the user_input really is correct. 


    
def looking_for(inventory):
    '''Method that prompts the user for a product_id. If it matches a product in the catalog, open google and search for the product name.'''
    quit_by_q = False # A bool to check for q-clicks
    return_value = None 
    search_name = None
    
    while (return_value is None and not quit_by_q): # Unless a product is found, loop. 
        print("") # For some spacing
        not_found = True # A checker for not finding the product, making it print once instead on every for loop.
        
        id_input = input("Please enter Product ID. For which to search the WEB for!")
        
        if id_input == "q" or id_input == "Q":
            quit_by_q = True
            break
            
        elif id_input.isalnum(): # Cuz we only accept letters and numbers for product_id. 
            
            for k, v in inventory.items(): 
                if id_input.upper() == v['product_id']: # If we have a match, store the product name in 'search_name'.
                    not_found = False
                    return_value = True
                    search_name = k
                    
            if not_found:
                print("The product ID was not found. Try another ID please.")
            else:
                pass
        else:
            print(f"Don't use special characters! Only a-z and 0-9, please.")
    
    if return_value: # If we have a match, enter below method with the correct product name. 
        search_for_input(search_name)

def search_for_input(name):
    '''Use webbrowser module to enter a new window with google and the product name.'''
    print("The item exists, let me GOOGLE that for you!")
    
    name.replace(" ", "+")
    print(name)
    webbrowser.open_new("https://www.google.com/search?q="+name)
    
    
def main():
    '''Firstly fetching the catalog, then comparing, then open google window.'''
    inventory_func = ViewCatalog.open_invent
    inventory = inventory_func()
    
    looking_for(inventory)
    
            
if __name__ == '__main__':
    main()