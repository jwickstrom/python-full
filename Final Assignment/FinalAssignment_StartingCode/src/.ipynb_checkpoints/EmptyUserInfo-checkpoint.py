import json

def store_premium_status():
    '''Method that aims to reset the file: user_info.json by changing the value from 1 to 0. This is because the user logs out and needs to re-purchase the Premium Pass'''
    k = "Premium Pass" 
    data = {k: {"valid": 0}}
    print("Logging out. Thank you for using __UKEA__!")
    
    with open("data/user_info.json", "w") as cart_out:
        json.dump(data, cart_out)

def main():
    store_premium_status()
       
if __name__ == '__main__':
    main()