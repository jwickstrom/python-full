'''ViewCatalog.py

A Python script written for UKEA Shopping.

A script that displays the current index of items'''

import os
import json 

def print_inventory_item(name,item):
    '''Pretty prints the given name and item to the console.
    Total max width of the line is 50. However, for design purposes we will need to add two "|" at both ends and an extra space in between. 
    Therefore we compensate these three by lower the max lenght below to 47.
    
    The print is adaptable, allowing the user to change the value on line 20 to adjust width.
    '''
    max_key_len = max([len(x) for x in item.keys()]) + 1
    max_val_len = max([len(str(x)) for x in item.values()]) + 1
    
    tot_max_size = 47 # Change this value if looking to increase width. # Default, 47.
    
    if max_val_len + max_key_len > tot_max_size:
        
        max_val_len = tot_max_size - max_key_len
    
    
    #print(len(f"|{name.upper()} "+"-"*(tot_max_size-len(name))+"|")) # Prints 50, for exactly perfect width.
    
    print("") # For some spacing
    print(f"|{name.upper()} "+"-"*(tot_max_size-len(name))+"|") # As this prints the Title of the inventory item, we implement method .upper() to name variable 
    
    for (k,v) in item.items():
        
        # Task 2.4 and 2.5,
        if k == "cost": 
            v = f"{ format(v, '.2f') } SEK"  # Adding the currency SEK to the cost item, also formating to 2 decimals.
        
        # Task 2.6
        if k == "dimensions": 
            v = f"{format(v[0], '.2f')} m x {format(v[1], '.2f')} m x {format(v[2], '.2f')} m" # Reformatting the value of the tuple, to a f-string with correct decimals.
         
        # Task 2.7 Depending on value of "instock", have different print-statements. 
        if k == "instock":
            v =  f'OUT OF STOCK' if v == 0 else f"{v} items left, lets hurry!!" if v < 5 else f"Less than {v} remaining!" if v < 10 else f"Only {v} left!" if v <= 20 else f"There are {v} currently in stock."
                
        # Task 2.2 and 2.3
        v_end = ""
        if type(v) is type("string"): # If type(v) is a string, check its' length
            v_end = ""
            while len(v)+len(k) >= tot_max_size: # While the full string is longer that "tot_max_size = 47". What words are we slicing?
                
                ending_index = v.rfind(' ') #Find the last/secong to last/etc spacebar. 
                
                v_end = v[(ending_index):] + v_end # Add to the previously empty string, v_end. # Important, add to the front! Otherwise words in opposite order!
                v = v[:ending_index] # The new base-string, without the ending. 

                          
        print(f"|{k.replace('_',' ').capitalize():<{max_key_len}}:{str(v):>{max_val_len}}|" if v_end == "" # If v_end hasn't changed, print as usual.
             else f'''|{k.replace('_',' ').capitalize():<{max_key_len}}:{str(v):>{max_val_len}}| 
| {v_end:>{max_key_len + max_val_len}}|''') # Printing v_end on new line, rightly-adjusted.

def open_invent(): 
    '''Method that reads the inventory.json file and turns it into dictionary for us to manipulate. 
    '''
    
    try:
        with open("data/inventory.json", "r") as invent_in:
            return json.load(invent_in)
    except:
        print("No file found")

def main():
    # Starting up print statement
    print("Viewing inventory...\n")
    
    inventory = open_invent() 
    
    # Print all items in inventory, in correct and fashionable way!
    for k,v in inventory.items():
        print_inventory_item(k, v) 
    
    # Print and empty string to make things spaced out
    print("")
    
if __name__ == '__main__':
    main()

