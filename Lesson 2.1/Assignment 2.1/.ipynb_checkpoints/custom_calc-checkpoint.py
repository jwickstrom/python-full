# custom_calc.py

'''A Module created for objective 4 in 2.1.2, executing some mathematical operations.'''

def addition(list): # Adding all numbers from the list. 
    result = sum(list)
    return result

def subtraction(list): # Function that subtracts the second value etc. with the first value. 
    result = list[0] # Declare result = first value in list, because subtracting from there, in loop
    for items in list[1:]: # Exclude the first value when initiating the loop
        result -= items
    return result

def multiplication(list): # Multiplication of all values
    result = 1 # Instead of doing as above, declare result=1, then tick trough the whole list below. 
    for items in list:
        result *= items
    return result

def divison(list): # Division from second value
    result = list[0] # Declare result as first value, then tick through whole list from second value
    for items in list[1:]:
        result /= items
    return result

def power(list): # Exponent all the values. 
    result = list[0] # Same logic as above
    for items in list[1:]:
        result **= items
    return result